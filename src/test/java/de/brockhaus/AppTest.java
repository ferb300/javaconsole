package de.brockhaus;

import java.util.Arrays;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }

    public void testHeapSort() {
        Integer[] array = {5, 9, 1, 5, 7, 0, 3, 6};

        Integer[] sortedArray = array.clone();
        Arrays.sort(sortedArray);

        Sorter<Integer> s = new Sorter<Integer>();
        s.heapSort(array);

        assertTrue("Arrays dont have same length!", array.length == sortedArray.length);
        for (int i = 0; i < array.length; i++) {
            assertEquals(sortedArray[i], array[i]);
        }
    }

    public void testBubbleSort() {
        Integer[] array = {5, 9, 1, 5, 7, 0, 3, 6};

        Integer[] sortedArray = array.clone();
        Arrays.sort(sortedArray);

        Sorter<Integer> s = new Sorter<Integer>();
        s.bubbleSort(array);

        assertTrue("Arrays dont have same length!", array.length == sortedArray.length);
        for (int i = 0; i < array.length; i++) {
            assertEquals(sortedArray[i], array[i]);
        }
    }

    public void testQuickSort() {
        Integer[] array = {5, 9, 1, 5, 7, 0, 3, 6};

        Integer[] sortedArray = array.clone();
        Arrays.sort(sortedArray);

        Sorter<Integer> s = new Sorter<Integer>();
        s.quickSort(array);

        assertTrue("Arrays dont have same length!", array.length == sortedArray.length);
        for (int i = 0; i < array.length; i++) {
            assertEquals(sortedArray[i], array[i]);
        }
    }

    public void testBinarySearch() {
        Integer[] array = {5, 9, 1, 7, 0, 3, 6};
        Arrays.sort(array);

        Searcher<Integer> s = new Searcher<Integer>();
        assertEquals("Wrong index returned!", 6, s.binarySearch(array, 9));
    }
}
 