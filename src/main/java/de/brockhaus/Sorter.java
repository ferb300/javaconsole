package de.brockhaus;

public class Sorter<T extends Comparable<T>> {

    public void heapSort(T[] arr) {
        if (arr == null) {
            throw new IllegalArgumentException("Array was null");
        }

        // build the max heap by letting values from the front half sicker in (back half
        // is trivially a max heap as no element has children)
        for (int i = arr.length / 2; i >= 0; i--) {
            reheap(arr, i, arr.length - 1);
        }

        // build the sorted sequence at the back of the array by taking max element and
        // reheaping
        T currValue;
        for (int i = arr.length - 1; i >= 0; i--) {
            // max value is put to the end of the array
            currValue = arr[i];
            arr[i] = arr[0];
            arr[0] = currValue;

            // reheaping and excluding array end
            reheap(arr, 0, i - 1);
        }
    }

    public void bubbleSort(T[] arr) {
        if (arr == null) {
            throw new IllegalArgumentException("Array was null");
        }

        boolean swapped = false;
        T curr;
        // swap neighbouring values till none need to be swapped
        do {
            swapped = false;
            for (int i = 0; i < arr.length - 1; i++) {
                if (arr[i].compareTo(arr[i + 1]) > 0) {
                    curr = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = curr;
                    swapped = true;
                }
            }
        } while (swapped);
    }

    // wrapper method for uncomplicated calling
    public void quickSort(T[] arr) {
        if (arr == null) {
            throw new IllegalArgumentException("Array was null");
        }
        quickSortRec(arr, 0, arr.length - 1);
    }

    // inner method for quicksort suitable for recursive calling
    private void quickSortRec(T[] arr, int startIndex, int endIndex) {
        if (endIndex > startIndex) {
            int pivot = partition(arr, startIndex, endIndex); // partition array
            quickSortRec(arr, startIndex, pivot - 1); // recursive calls for both halfs
            quickSortRec(arr, pivot + 1, endIndex);
        }
    }

    // Partitioning for quick sort: splits array into two halfs around pivot element
    // (with larger and smaller than pivot elements) and returns pivot index
    //
    // larger than pivot elements   smaller than pivot elements     elements still to assign
    // [startIndex, i]              ]i, j[                          [j, n[ 
    private int partition(T[] arr, int startIndex, int endIndex) {
        int i = startIndex - 1;
        int j = startIndex;
        T curr;
        while (j < endIndex) {
            // smaller than or equal to pivot elements are swapped into the first half
            if (arr[j].compareTo(arr[endIndex]) <= 0) {
                i++;
                curr = arr[i];
                arr[i] = arr[j];
                arr[j] = curr;
            }
            j++;
        }
        // pivot element is swapped between the two halfs and index is returned
        curr = arr[i + 1];
        arr[i + 1] = arr[endIndex];
        arr[endIndex] = curr;
        return i + 1;
    }

    // Value Sickering (reheap) for heap sort
    private void reheap(T[] arr, int index, int lastIndex) {
        int leftChildIndex = 2 * index + 1;
        int rightChildIndex = 2 * index + 2;
        T child;

        if (leftChildIndex > lastIndex) {
            return;
        }

        if ((rightChildIndex > lastIndex || arr[leftChildIndex].compareTo(arr[rightChildIndex]) > 0)
                && arr[leftChildIndex].compareTo(arr[index]) > 0) {
            // left Child ist bigger than right child or right Child doesnt exist
            // and left child is bigger than parant
            child = arr[leftChildIndex];
            arr[leftChildIndex] = arr[index];
            arr[index] = child;
            reheap(arr, leftChildIndex, lastIndex);
        } else if (rightChildIndex <= lastIndex && arr[rightChildIndex].compareTo(arr[index]) > 0) {
            // right child is bigger than parent
            child = arr[rightChildIndex];
            arr[rightChildIndex] = arr[index];
            arr[index] = child;
            reheap(arr, rightChildIndex, lastIndex);
        }
    }
}
