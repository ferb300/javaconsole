package de.brockhaus;

public class Searcher<T extends Comparable<T>> {
    
    public int binarySearch(T[] arr, T value) {
        if (arr == null) {
            throw new IllegalArgumentException("Array was null");
        }
        return binarySearchRec(arr, value, 0, arr.length);
    }

    private int binarySearchRec(T[] arr, T value, int start, int end) {
        int pivotIndex = (int) (start + 0.5 * (end - start));
        if (arr[pivotIndex].compareTo(value) > 0) {
            return binarySearchRec(arr, value, start, pivotIndex);
        } else if (arr[pivotIndex].compareTo(value) < 0) {
            return binarySearchRec(arr, value, pivotIndex, end);
        } else {
            return pivotIndex;
        }
    }
}
